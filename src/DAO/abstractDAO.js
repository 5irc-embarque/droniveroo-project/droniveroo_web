class AbstractDAO {

	/**
	 *
	 * @param {JsonDB} database Common database
	 * @param {String} name Name of the DAO
	 * @param {winston.logger} logger Common logger
	 */
	constructor(database, name, logger) {
		// Prevent the Databaseable class to be instantiated as it is abstract
		if (new.target === AbstractDAO) {
			throw new TypeError('Cannot construct AbstractDAO instances directly');
		}
		this.database = database;
		this.name = name;
		this.logger = logger;
		this.logger.info(`[${this.name}DAO] Ready`);
	}

	/**
	 * Get all the deliverables
	 */
	getAll() {
		let data = this.database.getData(`/${this.name}`);
		return (data);
	}

	/**
	 * Get a deliverable by its id
	 * @param {Number} id Id of the searched deliverable
	 */
	getById(id) {
		let data = this.database.getData(`/${this.name}/${id}`);
		return (data);
	}

	/**
	 * Add an object to the database
	 * @param {Object} object The object to add into the database
	 */
	add(object) {
		if (object.id !== undefined) {
			let idExists = true;
			try {
				this.getById(object.id);
			} catch (error) {
				idExists = false;
			}
			if (idExists) {
				throw new TypeError('Id already exists');
			} else {
				try {
					this.logger.debug(`[${this.name}DAO] Create %o`, object);
					this.database.push(`/${this.name}/${object.id}`, object);
				} catch (error) {
					throw error;
				}
			}
		} else {
			throw new TypeError('Object has no id');
		}
	}

	/**
	 * Update an object of the database given its id
	 * @param {Object} object The object to update
	 */
	update(object) {
		if (object.id !== undefined) {
			let idExists = true;
			try {
				this.getById(object.id);
			} catch (error) {
				idExists = false;
			}
			if (!idExists) {
				throw new TypeError('Id does not exists');
			} else {
				try {
					this.logger.debug(`[${this.name}DAO] Update %o`, object);
					this.database.push(`/${this.name}/${object.id}`, object);
				} catch (error) {
					throw error;
				}
			}

		} else {
			throw new TypeError('Object has no id');
		}
	}

	/**
	 * Delete a database entry
	 * @param {Number} id The object to delete id
	 */
	delete(id) {
		if (id !== undefined) {
			let idExists = true;
			try {
				this.getById(id);
			} catch (error) {
				idExists = false;
			}
			if (!idExists) {
				throw new TypeError('Id does not exists');
			} else {
				try {
					this.logger.debug(`[${this.name}DAO] Delete %s`, id);
					this.database.delete(`/${this.name}/${id}`);
				} catch (error) {
					throw error;
				}
			}
		} else {
			throw new TypeError('Object has no id');
		}
	}

}

module.exports = AbstractDAO;