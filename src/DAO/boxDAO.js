const AbstractDAO = require('./abstractDAO');

class BoxDAO extends AbstractDAO {

	/**
	 *
	 * @param {JsonDB} database Common database
	 * @param {winston.logger} logger Common logger
	 */
	constructor(database, logger) {
		super(database, 'Box', logger);
	}

}

module.exports = BoxDAO;