const AbstractDAO = require('./abstractDAO');

class DeliverableDAO extends AbstractDAO {

	/**
	 *
	 * @param {JsonDB} database Common database
	 * @param {winston.logger} logger Common logger
	 */
	constructor(database, logger) {
		super(database, 'Deliverable', logger);
	}

}

module.exports = DeliverableDAO;