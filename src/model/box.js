const uuidv4 = require('uuid/v4');

class Box {

	/**
	 *
	 * @param {String} name Deliverable's name
	 * @param {Object} droneLocation Location of box for drone point of view
	 * @param {Object} turtlebotLocation Location of box for turtlebot point of view
	 */
	constructor(name, droneLocation, turtlebotLocation) {
		this.id = uuidv4();
		this.name = name;
		this.droneLocation = droneLocation;
		this.turtlebotLocation = turtlebotLocation;
	}
}

module.exports = Box;