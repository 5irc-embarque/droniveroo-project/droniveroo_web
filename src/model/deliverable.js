const uuidv4 = require('uuid/v4');

class Deliverable {

	/**
	 *
	 * @param {String} name Deliverable's name
	 * @param {String} address Address to deliver the deliverable
	 */
	constructor(name, address) {
		this.id = uuidv4();
		this.name = name;
		this.address = address;
		this.registered = false;
		this.delivered = false;
	}
}

module.exports = Deliverable;