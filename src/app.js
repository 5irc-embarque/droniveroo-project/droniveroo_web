#!/usr/bin/env node

'use strict';

const readConfig = require('read-config');
const npmConfig = readConfig('package.json');
const logger = require('./utils/logger')('silly', 'WEB', 'combined.log');
const router = require('./router.js');
const express = require('express');
const JsonDB = require('node-json-db');
const DeliverableDAO = require('./DAO/deliverableDAO');
const BoxDAO = require('./DAO/boxDAO');
const Controller = require('./controller/controller');

logger.info('============================');
logger.info('Droniveroo web service');
logger.info('Version : %s', npmConfig.version);
logger.info('============================');
logger.info('[System] Starting');


let db = new JsonDB('./db.json', true, false);
let deliverableDAO = new DeliverableDAO(db, logger);
let boxDAO = new BoxDAO(db, logger);

let controller = new Controller(deliverableDAO, boxDAO, logger);

let app = express();
app.use(express.json());
let server = require('http').Server(app);
router.initialiseRoutes(app, controller, logger);
server.listen(1337);
logger.info('[System] Server listening on port 1337');
logger.info('[System] Ready');