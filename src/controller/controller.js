const Deliverable = require('../model/deliverable');
const axios = require('axios');
const boxIP = 'http://192.168.1.169';

class Controller {

	/**
	 *
	 * @param {DeliverableDAO} database Deliverable DAO
	 * @param {BoxDAO} database Box DAO
	 * @param {winston.logger} logger Common logger
	 */
	constructor(deliverableDAO, boxDAO, logger) {
		this.deliverableDAO = deliverableDAO;
		this.boxDAO = boxDAO;
		this.deliverablesSecureKeys = [];
		this.logger = logger;
		this.logger.info('[Controller] Ready');
	}

	/**
	 * Get all the deliverables
	 */
	getAllDeliverable() {
		let data;
		let res = [];
		try {
			data = this.deliverableDAO.getAll();
		} catch (error) {
			this.logger.error('[Controller] Error : %s', error);
			return res;
		}
		for (let i in data) {
			data[i].box = this.getBoxById(data[i].address);
			res.push(data[i]);
		}
		return res;
	}

	/**
	 * Get deliverable by its id
	 * @param {Number} id Id of the searched deliverable
	 */
	getDeliverableById(id) {
		let data;
		let res = {};
		try {
			data = this.deliverableDAO.getById(id);
		} catch (error) {
			this.logger.error('[Controller] Error : %s', error);
			return res;
		}
		data.box = this.getBoxById(data.address);
		res = data;
		return res;
	}

	/**
	 * Create and add a deliverable
	 * @param {String} name Name of the deliverable
	 * @param {String} address Address of the deliverable
	 */
	addDeliverable(name, address) {
		if (typeof name !== 'string' || name === '') {
			throw new TypeError('Name must be a string and can not be empty');
		} else if (typeof address !== 'string' || address === '') {
			throw new TypeError('Address must be a string and can not be empty');
		} else {
			let deliverable = new Deliverable(name, address);
			try {
				this.deliverableDAO.add(deliverable);
				return deliverable;
			} catch (error) {
				throw error;
			}
		}
	}

	/**
	 * Update a deliverable given its id
	 * @param {string} id Id of the deliverable to update
	 * @param {string} name Name to update
	 * @param {string} address Address to update
	 * @param {boolean} registered Registered state to update
	 * @param {boolean} delivered Delivered state to update
	 */
	updateDeliverable(id, name, address, registered, delivered) {
		if (typeof id !== 'string' || id === '') {
			throw new TypeError('Id must be a string and can not be empty');
		}
		let deliverable = this.getDeliverableById(id);
		if (Object.keys(deliverable).length > 0) {
			if (name !== undefined) {
				if (typeof name !== 'string' || name === '') {
					throw new TypeError('Name must be a string and can not be empty');
				} else {
					deliverable.name = name;
				}
			}

			if (address !== undefined) {
				if (typeof address !== 'string' || address === '') {
					throw new TypeError('Address must be a string and can not be empty');
				} else {
					deliverable.address = address;
				}
			}

			if (registered !== undefined) {
				if (typeof registered !== 'boolean' || registered === '') {
					throw new TypeError('Registered must be a boolean and can not be empty');
				} else {
					deliverable.registered = registered;
				}
			}

			if (delivered !== undefined) {
				if (typeof delivered !== 'boolean' || delivered === '') {
					throw new TypeError('Delivered must be a boolean and can not be empty');
				} else {
					deliverable.delivered = delivered;
				}
			}

			this.deliverableDAO.update(deliverable);
			return deliverable;
		} else {
			throw new TypeError('Id does not exists');
		}

	}

	/**
	 * Delete a deliverable
	 * @param {Number} id Deliverable to delete Id
	 */
	deleteDeliverable(id) {
		if (typeof id !== 'string' || id === '') {
			throw new TypeError('Id must be a string and can not be empty');
		}
		let deliverable = this.getDeliverableById(id);
		if (Object.keys(deliverable).length > 0) {
			this.deliverableDAO.delete(id);
			return;
		} else {
			throw new TypeError('Id does not exists');
		}
	}

	/**
	 * Get all the boxes
	 */
	getAllBoxes() {
		let data;
		let res = [];
		try {
			data = this.boxDAO.getAll();
		} catch (error) {
			this.logger.error('[Controller] Error : %s', error);
			return res;
		}
		for (let i in data) {
			res.push(data[i]);
		}
		return res;
	}

	/**
	 * Get box by its id
	 * @param {Number} id Id of the searched box
	 */
	getBoxById(id) {
		let data;
		let res = {};
		try {
			data = this.boxDAO.getById(id);
		} catch (error) {
			this.logger.error('[Controller] Error : %s', error);
			return res;
		}
		res = data;
		return res;
	}

	generateAndSendSecureKey(deliverableId) {
		// should be random
		let key = 'Nh4IpV4S8EHC4fcvL3Om485uzRu7e7vD6522';
		return axios.get(boxIP+'/qrcode/' + key).then((response) => {
			if (response.status === 200) {

				let keyObject = {};
				keyObject.id = deliverableId;
				keyObject.key = key;
				this.deliverablesSecureKeys.push(keyObject);
				return key;
			} else {
				this.logger.debug(`[Controller] Error : ${response.statusText}`);
				return null;
			}
		}).catch(function (error) {
			this.logger.debug(`[Controller] Error : ${error}`);
			return null;
		});
	}

	testSendSecureKey(deliverableId, code) {
		let test = this.deliverablesSecureKeys.find((element) => {
			return (element.id === deliverableId && element.key === code);
		});
		if (test !== undefined) {
			return this.openBox();
		} else {
			return false;
		}
	}

	openBox() {
		return axios.get(boxIP+'/openBox').then((response) => {
			if (response.status === 200) {
				setTimeout(this.closeBox, 15000);
				return true;
			} else {
				this.logger.debug(`[Controller] Error : ${response.statusText}`);
				return null;
			}
		}).catch(function (error) {
			this.logger.debug(`[Controller] Error : ${error}`);
			return null;
		});
	}

	closeBox() {
		return axios.get(boxIP+'/closeBox').then((response) => {
			if (response.status === 200) {
				return true;
			} else {
				this.logger.debug(`[Controller] Error : ${response.statusText}`);
				return null;
			}
		}).catch(function (error) {
			this.logger.debug(`[Controller] Error : ${error}`);
			return null;
		});
	}
}

module.exports = Controller;