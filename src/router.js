const apiBaseRoute = '/api/v1';
const QRCode = require('qrcode');

module.exports = {


	/**
	 * Initialises all the routes
	 * @param {Express} app The HTTP server
	 * @param {Controller} controller The Controller
	 * @param {winston.Logger} logger The logger
	 */
	initialiseRoutes: (app, controller, logger) => {
		logger.info('[Router] Starting');

		app.get(`${apiBaseRoute}/deliverables`, (req, res) => {
			logger.debug(`[Router] GET on ${apiBaseRoute}/deliverables`);
			let data = controller.getAllDeliverable();
			res.status(200).send(data);
		});

		app.get(`${apiBaseRoute}/deliverables/:deliverableId`, (req, res) => {
			logger.debug(`[Router] GET on ${apiBaseRoute}/deliverables/${req.params.deliverableId}`);
			let data = controller.getDeliverableById(req.params.deliverableId);
			// If the object exists
			if (Object.keys(data).length > 0) {
				res.status(200).send(data);
			} else {
				res.sendStatus(404);
			}
		});

		app.post(`${apiBaseRoute}/deliverables`, (req, res) => {
			logger.debug(`[Router] POST on ${apiBaseRoute}/deliverables/`);
			try {
				let data = controller.addDeliverable(req.body.name, req.body.address);
				res.status(201).send(data);
			} catch (error) {
				if (error instanceof TypeError) {
					res.status(422).send(error.toString());
				} else {
					res.status(400).send(error.toString());
				}
			}
		});

		app.put(`${apiBaseRoute}/deliverables/:deliverableId`, (req, res) => {
			logger.debug(`[Router] PUT on ${apiBaseRoute}/deliverables/${req.params.deliverableId}`);
			try {
				let data = controller.updateDeliverable(req.params.deliverableId, req.body.name, req.body.address, req.body.registered, req.body.delivered);
				res.status(200).send(data);
			} catch (error) {
				if (error instanceof TypeError) {
					if (error.message === 'Id does not exists') {
						res.status(404).send(error.toString());
					} else {
						res.status(422).send(error.toString());
					}
				} else {
					res.status(400).send(error.toString());
				}
			}
		});

		app.delete(`${apiBaseRoute}/deliverables/:deliverableId`, (req, res) => {
			logger.debug(`[Router] DELETE on ${apiBaseRoute}/deliverables/${req.params.deliverableId}`);
			try {
				controller.deleteDeliverable(req.params.deliverableId);
				res.status(200).send('Deliverable deleted');
			} catch (error) {
				if (error instanceof TypeError) {
					res.status(404).send(error.toString());
				} else {
					res.status(400).send(error.toString());
				}
			}
		});

		app.post(`${apiBaseRoute}/deliverables/:deliverableId/register`, (req, res) => {
			logger.debug(`[Router] POST on ${apiBaseRoute}/deliverables/${req.params.deliverableId}/register`);
			try {
				let data = controller.updateDeliverable(req.params.deliverableId, undefined, undefined, true, undefined);
				res.status(200).send(data);
			} catch (error) {
				if (error instanceof TypeError) {
					if (error.message === 'Id does not exists') {
						res.status(404).send(error.toString());
					} else {
						res.status(422).send(error.toString());
					}
				} else {
					res.status(400).send(error.toString());
				}
			}
		});

		app.post(`${apiBaseRoute}/deliverables/:deliverableId/received`, (req, res) => {
			logger.debug(`[Router] POST on ${apiBaseRoute}/deliverables/${req.params.deliverableId}/received`);
			try {
				let data = controller.updateDeliverable(req.params.deliverableId, undefined, undefined, undefined, true);
				res.status(200).send(data);
			} catch (error) {
				if (error instanceof TypeError) {
					if (error.message === 'Id does not exists') {
						res.status(404).send(error.toString());
					} else {
						res.status(422).send(error.toString());
					}
				} else {
					res.status(400).send(error.toString());
				}
			}
		});

		app.get(`${apiBaseRoute}/boxes`, (req, res) => {
			logger.debug(`[Router] GET on ${apiBaseRoute}/boxes`);
			let data = controller.getAllBoxes();
			res.status(200).send(data);
		});


		app.get(`${apiBaseRoute}/boxes/:boxId`, (req, res) => {
			logger.debug(`[Router] GET on ${apiBaseRoute}/box/${req.params.boxId}`);
			let data = controller.getBoxById(req.params.boxId);
			// If the object exists
			if (Object.keys(data).length > 0) {
				res.status(200).send(data);
			} else {
				res.sendStatus(404);
			}
		});

		app.get(`${apiBaseRoute}/deliverables/:deliverableId/qrcode`, (req, res) => {
			logger.debug(`[Router] GET on ${apiBaseRoute}/box/${req.params.deliverableId}/qrcode`);
			QRCode.toFile('./qrcode.png', '' + req.params.deliverableId, function (err) {
				if (err) {
					res.sendStatus(400);
				} else {

					var options = {
						root: __dirname + '/../',
					};
					res.status(200).sendFile('qrcode.png', options);
				}
			});
		});

		app.post(`${apiBaseRoute}/deliverables/:deliverableId/ready`, (req, res) => {
			logger.debug(`[Router] GET on ${apiBaseRoute}/deliverables/${req.params.deliverableId}/ready`);
			controller.generateAndSendSecureKey(req.params.deliverableId).then((keyRes) => {
				if(keyRes !== null){
					res.status(200).send(keyRes);
				}else{
					res.sendStatus(400);
				}
			});

		});

		app.post(`${apiBaseRoute}/deliverables/:deliverableId/readQRCode/:code`, (req, res) => {
			logger.debug(`[Router] GET on ${apiBaseRoute}/deliverables/${req.params.deliverableId}/readQRCode/${req.params.code}`);
			let testRes = controller.testSendSecureKey(req.params.deliverableId, req.params.code);
			if(testRes){
				res.sendStatus(200);
			}else{
				res.sendStatus(400);
			}

		});

		app.post(`${apiBaseRoute}/boxes/openBox`, (req, res) => {
			logger.debug(`[Router] POST on ${apiBaseRoute}/boxes/openBox`);
			let testRes = controller.openBox();
			if(testRes){
				res.sendStatus(200);
			}else{
				res.sendStatus(400);
			}

		});

		app.post(`${apiBaseRoute}/boxes/closeBox`, (req, res) => {
			logger.debug(`[Router] POST on ${apiBaseRoute}/boxes/closeBox`);
			let testRes = controller.closeBox();
			if(testRes){
				res.sendStatus(200);
			}else{
				res.sendStatus(400);
			}

		});


		app.get('/', (req, res) => {
			var options = {
				root: __dirname,
			};
			res.status(200).sendFile('index.html', options);
		});


		/**
		 * Default router
		 */
		app.use((req, res) => {
			logger.debug('[Router] 400 : access to %s', req.url);
			res.send(400);
		});

		logger.info('[Router] Ready');
	}
};