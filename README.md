# Droniveroo

Pour une description du projet Droniverro, se diriger vers le [readme principal](https://gitlab.com/5irc-embarque/droniveroo-project/droniveroo_ros).

Autre repository git :
- [Ros](https://gitlab.com/5irc-embarque/droniveroo-project/droniveroo_ros)
- [Box](https://gitlab.com/5irc-embarque/droniveroo-project/droniveroo_camera)


# Droniveroo Web

Cette partie du projet gère le serveur web en NodeJs. Il permet de faire le lien entre l'application web, la partie ROS (turtlebot et drone) et la boîte.

## Prérequis

Pour utiliser le serveur web, il faut avoir `nodejs` installé, en version 8 minimum.
Pour installer les plugins, se placer à la racine du projet et exécuter `npm install`.
La boîte doit se trouver sur le même réseau que le serveur et posséder l'IP `192.168.1.169` (pour modifier cette IP, modifier la constante `boxIP` dans le fichier `src/controller/controller.js`).

## Démarrage

Pour démarrer le serveur web, se placer à la racine du repository, et exécuter `src/app.js`. Le serveur écoutera sur le port 1337.
Une application web simple est disponible sur le port.


## Stockage de données

Le serveur stocke des données concernant les colis (deliverables) et les dépôts (boxes). Il se repose sur une base de données en Json comportant deux tables (`deliverables` et `box`). La table des dépôts contient les coordonnées de navigation pour le turtlebot et les coordonnées pour le drone.

## API

L'API est accessible à l'URL `/api/v1/`

### Colis

Les différentes routes d'accès sont :
- `GET` sur `/api/v1/deliverables` : récupère la liste des colis
- `GET` sur `/api/v1/deliverables/:deliverableID` : récupère un colis à partir de son uuid
- `POST` sur `/api/v1/deliverables` : créé un colis, avec les paramètres suivants à passer dans le body :
  - `name` - `string` : le nom du colis
  - `address` - `string` : l'uuid de la box associée
- `PUT` sur `/api/v1/deliverables/:deliverableID` : met à jour un colis, à parir de son uuid, avec les paramètres suivants à passer dans le body :
  - `name` - `string` : le nom du colis
  - `address` - `string` : l'uuid de la box associée
  - `registered` - `boolean` : l'état 'enregistré' du colis
  - `delivered` - `boolean` : l'état 'reçu' du colis
- `DELETE` sur `/api/v1/deliverables/:deliverableID` : supprime un colis, à parir de son uuid
- `POST` sur `/api/v1/deliverables/:deliverableID/registered` : met à jour l'état 'enregistré' du colis
- `POST` sur `/api/v1/deliverables/:deliverableID/received` : met à jour l'état 'reçu' du colis
- `GET` sur `/api/v1/deliverables/:deliverableID/qrcode` : génère et envoie un QR Code correspondant à l'uuid du colis
- `POST` sur `/api/v1/deliverables/:deliverableID/ready` : génère sauvegarde et envoie une clé de sécurité à la boîte
- `POST` sur `/api/v1/deliverables/:deliverableID/readQRCode/:code` : vérifie la cohérence entre un colis et un qrcode lu

### Dépôts

Les différentes routes d'accès sont :
- `GET` sur `/api/v1/boxes` : récupère la liste des dépôts
- `GET` sur `/api/v1/boxes/:boxId` : récupère un dépôt à partir de son uuid
- `GET` sur `/api/v1/boxes/openBox` : ouvre la boîte
- `GET` sur `/api/v1/boxes/closeBox` : ferme la boîte
